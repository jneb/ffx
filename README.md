# FFX #

An implementation of the FFX method of format preserving encryption
as specified in
https://csrc.nist.gov/CSRC/media/Projects/Block-Cipher-Techniques/documents/BCM/proposed-modes/ffx/ffx-spec.pdf

### Usage ###

This is a library that allows you to do format preserving encryption.
It is both useable as a readable version of the NIST document,
or as an implementation of the algorithm proper.
There are three versions on the scale from readable to reasonably fast.
The library contains three classes:
FFXnum: handling numbers
FFXstr: handling bytearrays (yes, that's a misnomer)
FastFFXnum: like FFXnum, but optimized for speed rather than readability

* Usage
For the FFXstr version, for example:
```
from hashlib import sha256
import ffx
key = sha256(b'Password').digest()[:16]
tweak = b'Jurjen'
encryptor = ffx.FFXstr(key, tweak)
print('Encrypting number with key', key.hex(), 'and tweak', tweak)
data = input('Give a decimal number:')
print('Encrypted number:' + encryptor.encrypt(data))
```

### How do I get set up? ###

* Installation
Make sure you have pycryptodome installed:
`pip install pycryptodome`
Put the library where it is reachable

* Dependencies
The Crypto library in Pypi's pycryptodome

* Self test
Running the file from the command line runs a self test and interactive demo

### Contribution guidelines ###

Feel free to contribute!

### Who do I talk to? ###

* Jurjen N.E. Bos