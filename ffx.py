#!python3
"""Encryption method FFX-A10 according to Bellare-Rogaway-Spies.
As specified in
https://csrc.nist.gov/CSRC/media/Projects/Block-Cipher-Techniques/documents/BCM/proposed-modes/ffx/ffx-spec.pdf
Jurjen Bos, feb 2011, improved july 2020.
"""

import sys
import struct
from math import log, ceil

from functools import wraps
from random import getrandbits, randrange


# free goodie: a universal debug decorator
def debug(f):
    "Decorator printing input and output of a function"
    @wraps(f)
    def wrapper(*args, **kwds):
        argdescr = ', '.join(list(map(repr, args)) +
                             list(map('{}={!r}'.format, list(kwds.items()))))
        print("Calling {}({})".format(f.__name__, argdescr))
        result = f(*args, **kwds)
        print("{}({}) returns {!r}".format(f.__name__, argdescr, result))
        return result
    return wrapper


try:
    import Crypto.Cipher.AES as AES
except ImportError:
    print("Sorry, you need the Crypto package.")
    print("try: pip install pycryptodome")
    sys.exit(1)

# functions for base conversion
def thread(n, radix, length):
    """Write n as a list of digits of given radix
    >>> thread(1234, 10, 5)
    [0, 1, 2, 3, 4]
    """
    result = []
    for _ in range(length):
        n, digit = divmod(n, radix)
        result.insert(0, digit)
    return result


def combine(l, radix):
    """Combine digits in list to number
    >>> combine([3, 1, 4, 1], 10)
    3141
    """
    result = 0
    for digit in l:
        result = radix*result + digit
    return result


class FFXnum:
    """The FFX as a numeric computation.
    The default parameter choice gives FFX-A10 as specified in Appendix B
    Setting radix=2, addition=0 gives FFX-A2 from Appendix A
    """
    packsBQ = struct.Struct('>7sBQ').pack
    unpackQQ = struct.Struct('>QQ').unpack
    packHBBBBBBQ = struct.Struct('>HBBBBBBQ').pack

    def __init__(self, K, T, radix=10, method=2, addition=1):
        self.K = K
        self.T = T
        self.n = None
        self.radix = radix
        self.method = method
        self.addition = addition
        self.initmethods(radix, method, addition)

    def initmethods(self, radix, method, addition):
        """Set digits, enc, dec, add, sub to the proper values.
        """
        self.digits = self.digits2 if radix == 2 else self.digitsR
        self.enc = {1: self.enc1, 2: self.enc2}[method]
        self.dec = {1: self.dec1, 2: self.dec2}[method]
        self.add = {0: self.add0, 1: self.add1}[addition]
        self.sub = {0: self.sub0, 1: self.sub1}[addition]

    def split(self, n):
        "Compute l for a given value of n"
        return n // 2

    def rnds(self, n):
        """Compute minimun number of rounds from the heuristic in the article.
        >>> f = FFXnum(b'J'*16, b'Test')
        >>> f.rnds(11)
        12
        >>> f.rnds(7)
        18
        >>> f = FFXnum(b'J'*16, b'Test', radix=2)
        >>> f.rnds(32)
        12
        >>> f.rnds(31), f.rnds(20)
        (18, 18)
        >>> f.rnds(19), f.rnds(14)
        (24, 24)
        """
        # the heuristic from the article
        entropyPerRound = self.split(n) * log(self.radix) / log(2)
        assert entropyPerRound >= 4, 'Number too short to securely encrypt'
        lowerbound = int(ceil(128. / entropyPerRound))
        lowerbound += 4
        lowerbound += -lowerbound % 6
        lowerbound = max(12, lowerbound)
        return lowerbound

    def setupF(self, n):
        """precomputation of common values for F
        >>> f = FFXnum(b'J'*16, b'Test')
        >>> f.setupF(9)
        >>> AES.new(b'J'*16, AES.MODE_ECB).decrypt(f.IV).hex()
        '000102010a0904120000000000000004'
        >>> f.prefixLastBlock[:4]
        b'Test'
        >>> len(f.prefixLastBlock)
        7
        """
        VERS = 1
        self.n = n
        # precompute number of rounds
        self.r = self.rnds(n)
        assert self.r & 1 == 0, "Number of rounds must be even, not %d" % n
        P = self.packHBBBBBBQ(
            VERS,
            self.method,
            self.addition,
            self.radix,
            n,
            self.split(n),
            self.r,
            len(self.T))
        # the round function encrypts prefix + i + B
        prefix = P + self.T + b'\x00'*((-len(self.T) - 9) % 16)
        assert len(prefix) % 16 == 7
        # precompute IV for last block from initial blocks
        self.IV = AES.new(self.K, AES.MODE_CBC, IV=bytes(16)).encrypt(
            prefix[:-7])[-16:]
        self.prefixLastBlock = prefix[-7:]
        # to make a number of digits64 digits with neglible unbalance
        # you can take a 64 bits number modulo radixpower
        self.digits64 = int(32*log(2)/log(self.radix))
        self.radixpower = self.radix ** self.digits64

    def F(self, i, B):
        """Round function
        >>> f = FFXnum(b'J'*16, b'Test')
        >>> f.setupF(9)
        >>> f.F(0, 12345)
        3391
        """
        # make an encryptor for the last block
        encryptor = AES.new(self.K, AES.MODE_CBC, self.IV)
        # construct last block(s) of encryption
        Q = self.packsBQ(self.prefixLastBlock, i, B)
        # determine number of digits of output
        m = self.split(self.n)
        if i & 1:
            m = self.n - m
        yp, ypp = self.unpackQQ(encryptor.encrypt(Q))
        return self.digits(m, yp, ypp)

    def digits2(self, m, yp, ypp):
        """Make output number from two 64 bit numbers
        >>> f = FFXnum(b'J'*16, b'Test', radix=2)
        >>> f.setupF(9)
        >>> hex(f.digits(15, getrandbits(64), 0xf234))
        '0x7234'
        """
        return (yp << 64 | ypp) & (1 << m) - 1

    def digitsR(self, m, yp, ypp):
        """Make output of m digits
        >>> f = FFXnum(b'J'*16, b'Test', radix=10)
        >>> f.setupF(9)
        >>> f.digits(3, None, 12345678)
        678
        >>> f.digits(12, 123456789, 33333333987654321)
        789987654321
        """
        # the blocks yp and ypp can be considered 64 bits of random
        # digits64 is constructed to get neglible unbalance
        if m <= self.digits64:
            return ypp % self.radix ** m
        return yp % self.radix ** (m - self.digits64)*self.radixpower + \
            ypp % self.radixpower

    def encrypt(self, n, X):
        "Encrypt n digit number"
        assert 0 <= X < self.radix ** n
        if self.n != n:
            self.setupF(n)
        l = self.split(n)
        radixl = self.radix ** l
        radixnl = self.radix ** (n - l)
        return self.enc(X, l, radixl, radixnl)

    def enc1(self, X, l, radixl, radixnl):
        """Internal encrypt with method 1
        >>> f = FFXnum(b'J'*16, b'Test', method=1)
        >>> f.encrypt(4, 2345)
        9882
        """
        for i in range(self.r):
            A, B = divmod(X, radixnl)
            C = self.add(l, A, self.F(i, B))
            X = B*radixl + C
        return X

    def enc2(self, X, l, radixl, radixnl):
        """Internal encrypt with method 2
        >>> f = FFXnum(b'J'*16, b'Test', method=2)
        >>> f.encrypt(4, 2345)
        7549
        """
        A, B = divmod(X, radixnl)
        lenA = l
        for i in range(self.r):
            # Lengths of A,B,C if i is even: l, n-l, l
            C = self.add(lenA, A, self.F(i, B))
            A, B = B, C
            lenA = self.n - lenA
        return A*radixnl + B

    def decrypt(self, n, Y):
        "Decrypt n digit number"
        assert 0 <= Y < self.radix ** n
        if self.n != n:
            self.setupF(n)
        l = self.split(n)
        radixl = self.radix ** l
        radixnl = self.radix ** (n - l)
        return self.dec(Y, l, radixl, radixnl)

    def dec1(self, Y, l, radixl, radixnl):
        """Internal decrypt with method 1
        >>> f = FFXnum(b'J'*16, b'Test', method=1)
        >>> f.decrypt(4, 9882)
        2345
        """
        for i in range(self.r-1, -1, -1):
            B, C = divmod(Y, radixl)
            A = self.sub(l, C, self.F(i, B))
            Y = A*radixl + B
        return Y

    def dec2(self, Y, l, radixl, radixnl):
        """Internal decrypt with method 2
        >>> f = FFXnum(b'J' * 16, b'Test', method=2)
        >>> f.decrypt(4, 7549)
        2345
        """
        A, B = divmod(Y, radixnl)
        lenA = l
        for i in range(self.r-1, -1, -1):
            lenA = self.n - lenA
            C, B = B, A
            # Lengths of A,B,C if i is even: l, n-l, l
            A = self.sub(lenA, C, self.F(i, B))
        return A * radixnl + B

    def add0(self, m, x, y):
        """characterwise addition of m digits
        >>> f = FFXnum(b'J' * 16, b'Test', addition=0)
        >>> f.add(4, 1234, 5678)
        6802
        """
        if self.radix == 2:
            return x ^ y
        return combine(
            ((a + b) % self.radix
             for a, b in zip(
                thread(x, self.radix, m),
                thread(y, self.radix, m))),
            self.radix)

    def add1(self, m, a, b):
        """blockwise addition of length l
        >>> f = FFXnum(b'J' * 16, b'Test', addition=1)
        >>> f.add(4, 1234, 5678)
        6912
        """
        return (a + b) % self.radix ** m

    def sub0(self, m, x, y):
        """characterwise subtraction of m digits
        >>> f = FFXnum(b'J' * 16, b'Test', 10, 1, 0)
        >>> f.sub(4, 6802, 5678)
        1234
        """
        if self.radix == 2:
            return x ^ y
        return combine(
            ((a - b) % self.radix
             for a, b in zip(
                thread(x, self.radix, m),
                thread(y, self.radix, m))),
            self.radix)

    def sub1(self, m, a, b):
        """blockwise subtraction of length l
        >>> f = FFXnum(b'J' * 16, b'Test', addition=1)
        >>> f.sub(4, 6912, 5678)
        1234
        """
        return (a - b) % self.radix ** m


class FFXstr(FFXnum):
    """ffx encryption, using strings with digits.
    Complies more with the definition of the document, but the code is ugly.
    Note: this is very inefficient for radix 2, FFXnum works better than.
    """
    digitTable = '0123456789abcdefghijklmnopqrstuvwxyz'

    def setupF(self, n):
        "Precomputation of common values for F"
        FFXnum.setupF(self, n)
        self.l = self.split(n)

    def F(self, i, B):
        """The round function of string B for round i, returning a string
        >>> f = FFXstr(b'J' * 16, b'Test')
        >>> f.setupF(9)
        >>> f.F(0, '12345')
        '3391'
        """
        # make an encryptor for the last block
        encryptor = AES.new(self.K, AES.MODE_CBC, self.IV)
        # construct last block(s) of encryption
        Q = self.packsBQ(self.prefixLastBlock, i, self.str2num(B))
        # determine number of digits of output
        m = self.split(self.n)
        if i & 1:
            m = self.n - m
        return self.digits(m, encryptor.encrypt(Q))

    def digits2(self, m, y):
        """Make output number string from AES block
        >>> f = FFXstr(b'J' * 16, b'Test', radix=2)
        >>> f.setupF(9)
        >>> f.digits(15, '\x55' * 14 + '\xf2\x34')
        '111001000110100'
        """
        chars = (m - 1) // 8 + 1
        return ('{:08b}' * chars).format( * list(map(ord, y[-chars:])))[-m:]

    def digitsR(self, m, y):
        """Make output of m digits from AES block
        >>> f = FFXstr(b'J' * 16, b'Test', radix=10)
        >>> f.setupF(9)
        >>> f.digits(3, struct.pack('>QQ', 0, 12345678))
        '678'
        >>> f.digits(12, struct.pack('>QQ', 123456789, 33333333987654321))
        '789987654321'
        """
        # the blocks yp and ypp can be considered 64 bits of random
        # digits64 is constructed to get neglible unbalance
        yp, ypp = self.unpackQQ(y)
        if m <= self.digits64:
            return self.num2str(ypp % self.radix ** m, m)
        return self.num2str(
            yp % self.radix ** (m - self.digits64), m - self.digits64) + \
            self.num2str(
                ypp % self.radixpower, self.digits64)

    def encrypt(self, X):
        "Encrypt data X"
        if self.n != len(X):
            self.setupF(len(X))
        return self.enc(X)

    def enc1(self, X):
        """Internal encrypt with method 1
        >>> f = FFXstr(b'J'*16, b'Test', method=1)
        >>> f.encrypt('2345')
        '9882'
        """
        for i in range(self.r):
            A, B = X[:self.l], X[self.l:]
            C = self.add(A, self.F(i, B))
            X = B + C
        return X

    def enc2(self, X):
        """Internal encrypt with method 2
        >>> f = FFXstr(b'J'*16, b'Test', method=2)
        >>> f.encrypt('2345')
        '7549'
        """
        A, B = X[:self.l], X[self.l:]
        for i in range(self.r):
            C = self.add(A, self.F(i, B))
            A, B = B, C
        return A + B

    def decrypt(self, Y):
        "Decrypt ciphertext Y"
        if self.n != len(Y):
            self.setupF(len(Y))
        return self.dec(Y)

    def dec1(self, Y):
        """Internal decrypt with method 1
        >>> f = FFXstr(b'J'*16, b'Test', method=1)
        >>> f.decrypt('9882')
        '2345'
        """
        for i in range(self.r-1, -1, -1):
            B, C = Y[:self.n - self.l], Y[self.n - self.l:]
            A = self.sub(C, self.F(i, B))
            Y = A + B
        return Y

    def dec2(self, Y):
        """Internal decrypt with method 2
        >>> f = FFXstr(b'J'*16, b'Test', method=2)
        >>> f.decrypt('7549')
        '2345'
        """
        A, B = Y[:self.l], Y[self.l:]
        for i in range(self.r-1, -1, -1):
            C, B = B, A
            A = self.sub(C, self.F(i, B))
        return A + B

    def add0(self, x, y):
        """characterwise addition
        >>> f = FFXstr(b'J'*16, b'Test', addition=0)
        >>> f.setupF(4)
        >>> f.add('1234', '5678')
        '6802'
        """
        return ''.join(str((int(a) + int(b)) % self.radix)
                       for a, b in zip(x, y))

    def add1(self, a, b):
        """blockwise addition
        >>> f = FFXstr(b'J'*16, b'Test', addition=1)
        >>> f.setupF(4)
        >>> f.add('1234', '5678')
        '6912'
        """
        l = len(a)
        assert l == len(b)
        return self.num2str(
            (self.str2num(a) + self.str2num(b)) % self.radix ** self.n,
            l)

    def sub0(self, x, y):
        """characterwise subtraction
        >>> f = FFXstr(b'J'*16, b'Test', addition=0)
        >>> f.sub('6802', '5678')
        '1234'
        """
        return ''.join(str((int(a) - int(b)) % self.radix)
                       for a, b in zip(x, y))

    def sub1(self, a, b):
        """blockwise subtraction
        >>> f = FFXstr(b'J'*16, b'Test', addition=1)
        >>> f.setupF(4)
        >>> f.sub('6912', '5678')
        '1234'
        """
        l = len(a)
        assert l == len(b)
        return self.num2str(
            (self.str2num(a) - self.str2num(b)) % self.radix ** self.n,
            l)

    def str2num(self, s):
        "Convert an n bytes string to a number"
        return int(s, self.radix)

    def num2str(self, x, l):
        "Convert a number to an l bytes string"
        # This should have been a Python builtin a long time ago.
        result = ""
        for _ in range(l):
            x, d = divmod(x, self.radix)
            result = self.digitTable[d] + result
        return result


class FastFFXnum(FFXnum):
    """Same as FFXnum, but optimised for Python speed
    at the cost of readability.
    """

    def __init__(self, K, T, radix=10, method=2, addition=1):
        FFXnum.__init__(self, K, T, radix, method, addition)
        # dict from n to (r, IV, prefixLastBlock, digits64, radixpower)
        self.setups = {}

    def F(self, i, B, m):
        """Round function
        >>> f = FastFFXnum(b'J'*16, b'Test')
        >>> f.setupF(9)
        >>> f.F(0, 12345, 4)
        3391
        """
        return self.digits(m, *self.unpackQQ(
            AES.new(self.K, AES.MODE_CBC, self.IV).encrypt(
                self.packsBQ(self.prefixLastBlock, i, B))))

    def encrypt(self, n, X):
        "Encrypt n digit number"
        if self.n != n:
            try:
                (l, self.r, self.IV, self.prefixLastBlock,
                    self.digits64, self.radixpower) = self.setups[n]
            except KeyError:
                # setup, and save results
                self.setupF(n)
                l = self.split(n)
                self.setups[n] = (l, self.r, self.IV, self.prefixLastBlock,
                                  self.digits64, self.radixpower)
        return self.enc(X, l, self.radix ** l, self.radix ** (n - l))

    def enc1(self, X, l, radixl, radixnl):
        """Internal encrypt with method 1
        >>> f = FastFFXnum(b'J'*16, b'Test', method=1)
        >>> f.encrypt(4, 2345)
        9882
        """
        add = self.add
        F = self.F
        for i in range(self.r):
            B = X % radixnl
            X = B*radixl + add(l, X // radixnl, F(i, B, l))
        return X

    def enc2(self, X, l, radixl, radixnl):
        """Internal encrypt with method 2
        >>> f = FastFFXnum(b'J'*16, b'Test', method=2)
        >>> f.encrypt(4, 2345)
        7549
        """
        A, B = X // radixnl, X % radixnl
        lenA = l
        add = self.add
        F = self.F
        nml = self.n - l
        for i in range(0, self.r, 2):
            # Lengths of A,B,C if i is even: l, n-l, l
            C = add(l, A, F(i, B, l))
            # removed A,B = B,C so we say B to A and C to B
            A, B = C, add(nml, B, F(i+1, C, nml))
        return A*radixnl + B

    def decrypt(self, n, Y):
        "Decrypt n digit number"
        if self.n != n:
            try:
                (l, self.r, self.IV, self.prefixLastBlock,
                    self.digits64, self.radixpower) = self.setups[n]
            except KeyError:
                # setup, and save results
                self.setupF(n)
                l = self.split(n)
                self.setups[n] = (l, self.r, self.IV, self.prefixLastBlock,
                                  self.digits64, self.radixpower)
        return self.dec(Y, l, self.radix ** l, self.radix ** (n - l))

    def dec1(self, Y, l, radixl, radixnl):
        """Internal decrypt with method 1
        >>> f = FastFFXnum(b'J'*16, b'Test', method=1)
        >>> f.decrypt(4, 9882)
        2345
        """
        sub = self.sub
        F = self.F
        for i in range(self.r-1, -1, -1):
            B = Y // radixl
            Y = sub(l, Y % radixl, F(i, B, l))*radixl + B
        return Y

    def dec2(self, Y, l, radixl, radixnl):
        """Internal decrypt with method 2
        >>> f = FastFFXnum(b'J'*16, b'Test', method=2)
        >>> f.decrypt(4, 7549)
        2345
        """
        A, B = Y // radixnl, Y % radixnl
        lenA = l
        sub = self.sub
        F = self.F
        nml = self.n - l
        for i in range(self.r-1, -1, -2):
            # removed C,B = B,A so we say B to C and A to B
            B = self.sub(nml, B, F(i, A, nml))
            # removed C,B = B,A again so we now say A to C and B is already OK
            A = self.sub(l, A, F(i-1, B, l))
        return A*radixnl + B

    def add0(self, m, x, y):
        """characterwise addition
        >>> f = FastFFXnum(b'J'*16, b'Test', addition=0)
        >>> f.setupF(4)
        >>> f.add(4, 1234, 5678)
        6802
        """
        radix = self.radix
        if radix == 2:
            return x ^ y
        # the value that a carry of the current inspected would add
        carrypos = 1
        # the result value, with extra carries
        result = x + y
        while x or y:
            # move carry position to current digit
            carrypos *= radix
            # subtract carry if sum doesn't fit in a digit (divmod is slow)
            if x % radix + y % radix >= radix:
                result -= carrypos
            # move to next position
            x //= radix
            y //= radix
        return result

    def sub0(self, m, x, y):
        """characterwise subtraction of m digits
        >>> f = FastFFXnum(b'J'*16, b'Test', 10, 1, 0)
        >>> f.sub(4, 6802, 5678)
        1234
        """
        radix = self.radix
        if radix == 2:
            return x ^ y
        # the value that a carry of the current inspected would add
        carrypos = 1
        # the result value, with extra carries
        result = x - y
        while x or y:
            # move carry position to current digit
            carrypos *= radix
            # add carry if digit sum gets negative
            if x % radix - y % radix < 0:
                result += carrypos
            # move to next position
            x //= radix
            y //= radix
        return result


if __name__ == '__main__':
    import doctest
    import ffx
    # self test
    doctest.testmod(ffx)

    # sample usage
    from hashlib import sha256
    key = sha256(b'Password').digest()[:16]
    tweak = b'equensWorldline'
    encryptor = FFXstr(key, tweak)
    print('Encrypting number with key', key.hex(), end=' ')
    print('and tweak', tweak)
    while 1:
        data = input('Give a decimal number:')
        if not data:
            break
        try:
            print('Encrypted number:' + encryptor.encrypt(data))
        except AssertionError as message:
            print(message)
